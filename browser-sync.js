// Hack for Ubuntu on Windows: interface enumeration fails with EINVAL, so return empty.
try {
  require('os').networkInterfaces()
} catch (e) {
  require('os').networkInterfaces = () => ({})
}

const browserSync = require('browser-sync');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const argv = require('yargs').argv;
const webpackConfig = require('./webpack.config.js')(argv);
const bundler = webpack(webpackConfig)

browserSync({
	server: {
		baseDir: 'src',
		middleware: [
			webpackDevMiddleware(bundler, {
				// IMPORTANT: dev middleware can't access config, so we should
				// provide publicPath by ourselves
				publicPath: webpackConfig.output.publicPath,

				// pretty colored output
				stats: { colors: true }

				// for other settings see
				// http://webpack.github.io/docs/webpack-dev-middleware.html
			}),

			// bundler should be the same as above
			webpackHotMiddleware(bundler)
		]
	},
	open: true
});