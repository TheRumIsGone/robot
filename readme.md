# Robot Simulator v 1.0.0

### Hi, Thanks for taking the time to go through this repo.

## Scripts

* ```npm run build``` (generate webpack bundle)
* ```npm run dev``` (spin the browsersync dev environment)
* ```npm run test``` (spin the test environment)
* ```npm run test:single``` (run the tests once and generate coverage)

The examples given in the problem have been executed in unit tests.