const path = require('path');

module.exports = path.join.bind(path, path.resolve(__dirname, '..')); //move one folder out to root directory of project