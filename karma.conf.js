// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html
// https://github.com/AngularClass/angular2-webpack-starter/blob/master/config/karma.conf.js

module.exports = function (config) {
	const testWebpackConfig = require('./webpack.test.js')();

	const configuration = {
		basePath: '',
		frameworks: ['jasmine'],
		client: {
			captureConsole: false // do not emit console messages to terminal
		},
		coverageReporter: {
			type: 'in-memory' // no output for remapping
		},
		remapCoverageReporter: {
			'text-summary': null,
			json: './coverage/coverage.json',
			html: './coverage/html'
		},
		files: [
			{ pattern: './config/spec-bundle.js', watched: false }, // using webpack, we get the rest of our tests using require context
		],
		preprocessors: { './config/spec-bundle.js': ['coverage', 'webpack', 'sourcemap'] },
		webpack: testWebpackConfig,
		webpackMiddleware: {
			noInfo: true,
			stats: {
				chunks: false
			}
		},
		coverageIstanbulReporter: {
			reports: [ 'html', 'lcovonly', 'text'],
			fixWebpackSourcePaths: true,
			dir: './coverage',
			includeAllSources: true
		},
		reporters: ['progress', 'coverage', 'remap-coverage'],
		port: 9876,
		colors: true,
		logLevel: config.LOG_WARN,
		autoWatch: false,
		browsers: [
			'Chrome'
		]
	}

	config.set(configuration);
};
