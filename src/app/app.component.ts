import { Component } from '@angular/core';
import { AppStore, PacMan, State } from './app-store';

@Component({
	selector: 'app',
	templateUrl: 'app.template.html'
})
export class AppComponent {
	state: State;
	pacMan: PacMan = {
		xPos: 0,
		yPos: 0,
		bearing: 'N'
	};
	placePacman;
	movePacman;
	turnPacman;
	reportPacman;

	constructor(public appStore: AppStore) {
		appStore.changes.subscribe((store) => {
			this.state = store;
		})

		this.placePacman = appStore.placePacman;
		this.movePacman = appStore.movePacman;
		this.turnPacman = appStore.turnPacman;
		this.reportPacman = appStore.reportPacman;
	}
}