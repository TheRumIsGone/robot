import { TestBed, inject } from '@angular/core/testing';
import { async, fakeAsync, tick } from '@angular/core/testing';

import { AppStore, PacMan } from './app-store';

describe('AppStore', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AppStore
			]
		});
	});

	it('should initialize the service', inject([AppStore], (service: AppStore) => {
		expect(service).toBeTruthy();
	}));

	describe('pacMan', () => {
		it('should not exist before placement', inject([AppStore], (service: AppStore) => {
			service.purge();
			expect(service.getState().pacMan).toBeFalsy();
		}));

		it('should return error object if reported before placement', inject([AppStore], (service: AppStore) => {
			service.purge();
			expect(service.reportPacman().error).toBeTruthy();
		}));

		it('should contain 3 properties after placement', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 1,
				bearing: 'S'
			}

			service.placePacman(pacMan);

			expect(service.getState().pacMan).toBeTruthy();
			expect(service.getState().pacMan.xPos).toBeTruthy();
			expect(service.getState().pacMan.yPos).toBeTruthy();
			expect(service.getState().pacMan.bearing).toBeTruthy();

		}));

		it('can be placed with all four bearing directions', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 1,
				bearing: 'S'
			}

			service.placePacman(pacMan);

			expect(service.getState().pacMan.bearing).toEqual('S');

			pacMan.bearing = 'N'
			service.placePacman(pacMan);

			expect(service.getState().pacMan.bearing).toEqual('N');

			pacMan.bearing = 'E'
			service.placePacman(pacMan);

			expect(service.getState().pacMan.bearing).toEqual('E');

			pacMan.bearing = 'W'
			service.placePacman(pacMan);

			expect(service.getState().pacMan.bearing).toEqual('W');

		}));

		it('Defaults to North with bad bearing', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 1,
				bearing: 'Something'
			}

			service.placePacman(pacMan);

			expect(service.getState().pacMan.bearing).toEqual('N');

		}));

		it('should be able to place with incompatible placement values', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: -1,
				yPos: 100,
				bearing: 'S'
			}

			service.placePacman(pacMan);

			expect(service.getState().pacMan).toEqual({
				xPos: 1,
				yPos: 5,
				bearing: 'S'
			});

			pacMan.xPos = 100;
			pacMan.yPos = -1;

			service.placePacman(pacMan);

			expect(service.getState().pacMan).toEqual({
				xPos: 5,
				yPos: 1,
				bearing: 'S'
			});
		}));

		it('should return the pacman along side a console log when reported', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 1,
				bearing: 'S'
			}

			service.placePacman(pacMan);

			expect(service.reportPacman().value).toEqual(pacMan);
		}));
	});

	describe('movePacman', () => {
		it('when facing North move until yPos 5, then stop', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 4,
				bearing: 'N'
			}

			service.placePacman(pacMan);
			expect(service.getState().pacMan.yPos).toEqual(4)
			service.movePacman();
			expect(service.getState().pacMan.yPos).toEqual(5)
			service.movePacman();
			expect(service.getState().pacMan.yPos).toEqual(5)

		}));

		it('when facing East move until xPos 5, then stop', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 4,
				yPos: 1,
				bearing: 'E'
			}

			service.placePacman(pacMan);
			expect(service.getState().pacMan.xPos).toEqual(4)
			service.movePacman();
			expect(service.getState().pacMan.xPos).toEqual(5)
			service.movePacman();
			expect(service.getState().pacMan.xPos).toEqual(5)

		}));

		it('when facing South move until yPos 0, then stop', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 1,
				bearing: 'S'
			}

			service.placePacman(pacMan);
			expect(service.getState().pacMan.yPos).toEqual(1)
			service.movePacman();
			expect(service.getState().pacMan.yPos).toEqual(0)
			service.movePacman();
			expect(service.getState().pacMan.yPos).toEqual(0)

		}));

		it('when facing West move until xPos 0, then stop', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 1,
				bearing: 'W'
			}

			service.placePacman(pacMan);
			expect(service.getState().pacMan.xPos).toEqual(1)
			service.movePacman();
			expect(service.getState().pacMan.xPos).toEqual(0)
			service.movePacman();
			expect(service.getState().pacMan.xPos).toEqual(0)

		}));
	});

	describe('turnPacman', () => {

		it('should loop from through N > E > S > W > N if turning Right', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 0,
				yPos: 0,
				bearing: 'N'
			};

			service.placePacman(pacMan);

			service.turnPacman('RIGHT')
			expect(service.getState().pacMan.bearing).toEqual('E');

			service.turnPacman('RIGHT')
			expect(service.getState().pacMan.bearing).toEqual('S')

			service.turnPacman('RIGHT')
			expect(service.getState().pacMan.bearing).toEqual('W')

			service.turnPacman('RIGHT')
			expect(service.getState().pacMan.bearing).toEqual('N')

			
			expect(service.turnPacman('Something')).toEqual({
				status: 'BAD',
				error: 'Invalid Bearing'
			})

		}));

		it('should loop from through N > W > S > E > N if turning Left', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 0,
				yPos: 0,
				bearing: 'N'
			};

			service.placePacman(pacMan);

			service.turnPacman('LEFT')
			expect(service.getState().pacMan.bearing).toEqual('W');

			service.turnPacman('LEFT')
			expect(service.getState().pacMan.bearing).toEqual('S')

			service.turnPacman('LEFT')
			expect(service.getState().pacMan.bearing).toEqual('E')

			service.turnPacman('LEFT')
			expect(service.getState().pacMan.bearing).toEqual('N')

		}));
	})

	describe('Example', () => {

		it('a:', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 0,
				yPos: 0,
				bearing: 'N'
			}

			service.placePacman(pacMan);
			service.movePacman()
			expect(service.reportPacman().value).toEqual({
				xPos: 0,
				yPos: 1,
				bearing: 'N'
			});
		}));

		it('b:', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 0,
				yPos: 0,
				bearing: 'N'
			}

			service.placePacman(pacMan);
			service.turnPacman("LEFT");
			expect(service.reportPacman().value).toEqual({
				xPos: 0,
				yPos: 0,
				bearing: 'W'
			});
		}));

		it('c:', inject([AppStore], (service: AppStore) => {
			const pacMan: PacMan = {
				xPos: 1,
				yPos: 2,
				bearing: 'E'
			}

			service.placePacman(pacMan);
			service.movePacman()
			service.movePacman()
			service.turnPacman("LEFT")
			service.movePacman()
			expect(service.reportPacman().value).toEqual({
				xPos: 3,
				yPos: 3,
				bearing: 'N'
			});
		}));
	})

});