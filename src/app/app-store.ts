import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/distinctUntilChanged';

export interface PacMan {
	xPos: number,
	yPos: number,
	bearing: string
}

export interface State {
// define your state here
	maxX: number,
	maxY: number,
	pacMan?: PacMan
}

export interface ReportObj {
	status: string,
	error: any,
	value?: PacMan
}

const defaultState: State = {
// define your initial state here
	maxX: 5,
	maxY: 5
}

const _store = new BehaviorSubject<State>(defaultState);

@Injectable()
export class AppStore {
	private _store = _store;
	changes = this._store
		.asObservable()
		.distinctUntilChanged()

	constructor() {
		this.placePacman = this.placePacman.bind(this);
		this.movePacman = this.movePacman.bind(this);
		this.turnPacman = this.turnPacman.bind(this);
		this.reportPacman = this.reportPacman.bind(this);
	}

	setState(state: State) {
		this._store.next(state);
	}

	getState(): State {
		return this._store.value;
	}

	purge() {
		this._store.next(defaultState);
	}

	placePacman({xPos, yPos, bearing}: PacMan) {
		const acceptableBearings = ['N', 'S', 'E', 'W'];
		bearing = acceptableBearings.indexOf(bearing) > -1 ? bearing : 'N';

		if ( xPos > this._store.value.maxX ) {
			xPos = this._store.value.maxX
		}

		if ( yPos > this._store.value.maxY ) {
			yPos = this._store.value.maxY
		}

		this.setState({
			...this._store.value,
			pacMan: {
				xPos: xPos > -1 ? xPos : 1,
				yPos: yPos > -1 ? yPos : 1,
				bearing
			}
		})
	}

	turnPacman(direction: string, {xPos, yPos, bearing}: PacMan = this._store.value.pacMan) {
		const clockWiseOrder = ['N', 'E', 'S', 'W'];
		let newBearing: string;
		let index: number = clockWiseOrder.indexOf(bearing);

		if (direction === 'LEFT') {
			newBearing = index ? clockWiseOrder[index - 1] : clockWiseOrder[clockWiseOrder.length - 1]
		} else if (direction === 'RIGHT') {
			newBearing = index === clockWiseOrder.length - 1 ? clockWiseOrder[0] : clockWiseOrder[index + 1]
		} else {
			let reportObj: ReportObj = {
				status: 'BAD',
				error: 'Invalid Bearing'
			};

			return reportObj;
		}

		this.setState({
			...this._store.value,
			pacMan: {
				xPos,
				yPos,
				bearing: newBearing
			}
		})
	}

	reportPacman( pacMan = this._store.value.pacMan): ReportObj {
		let reportObj: ReportObj;

		if (pacMan) {
			reportObj = {
				status: 'OK',
				error: false,
				value: pacMan
			}; // if we want to chain the output of the report to data services
		} else {
			reportObj = {
				status: 'BAD',
				error: 'No Pacman Placed'
			} // can return an error object if we want to track failed reports
		}

		console.log(reportObj.error ? reportObj.error : reportObj.value)
		return reportObj;
	}

	movePacman({xPos, yPos, bearing}: PacMan = this._store.value.pacMan) {
		let xChange: number = 0;
		let yChange: number = 0;

		switch (bearing) {
			case "N":
				yChange = yPos < this._store.value.maxY ? 1 : 0;
				break;
			case "W":
				xChange = xPos > 0 ? -1 : 0;
				break;
			case "E":
				xChange = xPos < this._store.value.maxX ? 1 : 0;
				break;
			case "S":
				yChange = yPos > 0 ? -1 : 0;
				break;
		}

		this.setState({
			...this._store.value,
			pacMan: {
				xPos: xPos + xChange,
				yPos: yPos + yChange,
				bearing
			}
		})
	}
}