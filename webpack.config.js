const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ContextReplacementPlugin, HotModuleReplacementPlugin } = webpack;

const root = require('./helpers/root');

module.exports = (env = {}) => {
	const config = {
		devtool: 'source-map',
		entry: (() => {
			const obj = {
				main: ['./src/main.ts']
			}

			if (env.hot) {
				obj.main = [
					'webpack/hot/dev-server',
					'webpack-hot-middleware/client',
					...obj.main
				]
			}

			return obj
		})(),
		output: {
			path: root('dist'),
			publicPath: '/',
			filename: env.hot ? '[hash][name].js' : '[chunkhash][name].js'
		},
		module: {
			rules: [
				{
					test: /\.ts$/,
					use: [
						{
							loader: '@angularclass/hmr-loader',
							options: {
								pretty: !env.prod,
								prod: env.prod
							}
						},
						{
							loader: 'awesome-typescript-loader',
							options: {
							  configFileName: 'tsconfig.dev.json' // exclude spec files for dev and build
							}
						},
						{
							loader: 'angular2-template-loader'
						}
					],
					exclude: [
						/\.(e2e|spec)\.ts$/,
						/node_modules/
					]
				},
				{
				  test: /\.(png|jpg|gif|svg)$/,
				  loader: 'file-loader',
				  options: {
				    name: '[name].[ext]?[hash]'
				  }
				},
				{ 
				  test: /\.(html|css)$/, 
				  loader: 'raw-loader'
				},
			]
		},
		plugins: [
			new HtmlWebpackPlugin({
				template: 'src/index.html',
				inject: 'body'
			}),
			new ContextReplacementPlugin(
				// The (\\|\/) piece accounts for path separators in *nix and Windows
				/angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
				path.join(__dirname, './src'), // location of your src
				{} // a map of your routes 
			),
		].concat(env.hot ? [
			new HotModuleReplacementPlugin()
		] : []),
		resolve: {
			extensions: ['.ts', '.js'],
			modules: [root('src'), root('node_modules')]
		}
	};

	return config;
}